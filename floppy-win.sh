#!/bin/sh
set -e
. ./build.sh

mkdir -p fddir
mkdir -p fddir/boot

cp sysroot/boot/kernel.elf fddir/boot/kernel.elf
cat > fddir/boot/menu.cfg << EOF
title	Boot PuckOS
root	(fd0)
kernel	/boot/kernel.elf
EOF


cp mktool/grub_disk floppy.img
imdisk -a -f floppy.img -s 1440K -m B:
cp -R fddir/* /cygdrive/b
imdisk -D -m B:

#!/bin/sh
set -e
. ./build.sh

mkdir -p fddir
mkdir -p fddir/boot

cp sysroot/boot/kernel.elf fddir/boot/kernel.elf
cat > fddir/boot/menu.cfg << EOF
title	Boot PuckOS
root	(fd0)
kernel	/boot/kernel.elf
EOF

cp mktool/grub_disk floppy.img
sudo losetup /dev/loop8 floppy.img
mkdir -p fdmnt
sudo mount /dev/loop8 fdmnt
sudo cp -R fddir/* fdmnt
sudo umount fdmnt
rm -rf fdmnt
sudo losetup -d /dev/loop8

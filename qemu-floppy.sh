#!/bin/sh
set -e
. ./floppy.sh

qemu-system-$(./target-triplet-to-arch.sh $HOST) -fda floppy.img -serial stdio

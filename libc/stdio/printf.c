#include <stdio.h>
#include <stdarg.h>
#include <kernel/kheap.h>
#include <kernel/tty.h>

int printf(const char *format, ...) {
	va_list params;
	va_start(params, format);
	uint32_t i = 0, j = 0, written = 0;
	char *str = kmalloc(4096);
	
	while(format[i] != '\0') {
		if(format[i] == '%') {
			i++;
			
			uint32_t len = 0;
			if(format[i] >= '0' && format[i] <= '9') {
				char *len_arr = kmalloc(12);
				uint32_t t = 0;
				while(format[i] >= '0' && format[i] <= '9') {
					len_arr[t++] = format[i++];
				}
				len_arr[i] = '\0';
				len = atoi(len_arr);
			}
			
			char flag = format[i++];
			
			if(flag == 'c') {
				char c = (char) va_arg(params, int);
				str[j++] = c;
			} else if(flag == 's') {
				const char *s = va_arg(params, const char *);
				for(uint32_t t = 0; t < strlen(s); t++) {
					str[j++] = s[t];
				}
			} else if(flag == '%') {
				str[j++] = '%';
			} else if(flag == 'n') {
				uint32_t *ptr = va_arg(params, uint32_t *);
				*ptr = j;
			} else if(flag == 'u') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char *num = kmalloc(100);
				itoa_unsigned(a, num, 10);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 10);
				for(uint32_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
				kfree(num);
			} else if(flag == 'd' || flag == 'i') {
				int a = va_arg(params, int);
				char num[100];
				itoa(a, num, 10);
				for(uint32_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
			} else if(flag == 'x') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char *num = kmalloc(100);
				itoa_unsigned(a, num, 16);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 16);
				for(uint32_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
				kfree(num);
			} else if(flag == 'X') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char *num = kmalloc(100);
				itoa_unsigned(a, num, 16);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 16);
				for(uint32_t t = 0; t < strlen(num); t++) {
					if(num[t] >= 'a') num[t] = num[t] - 'a' + 'A';
					str[j++] = num[t];
				}
				kfree(num);
			} else if(flag == 'p') {
				uint32_t a = va_arg(params, uint32_t *);
				uint32_t skip = 0;
				char *num = kmalloc(100);
				itoa_unsigned(a, num, 16);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 16);
				for(uint32_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
				kfree(num);
			} else if(flag == 'o') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char *num = kmalloc(100);
				itoa_unsigned(a, num, 8);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 8);
				for(uint32_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
				kfree(num);
			}
		} else {
			str[j++] = format[i++];
		}
	}
	str[j] = 0;
	
	tty_puts(str);
	kfree(str);
	va_end(params);
	return j;
}
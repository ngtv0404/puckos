#ifndef _STDIO_H
#define _STDIO_H 1
 
#include <sys/cdefs.h>
 
#include <stddef.h>
#include <stdint.h>
 
#ifdef __cplusplus
extern "C" {
#endif
 
int putchar(int ic);
int puts(const char *string);
int sprintf(char *str, const char *format, ...);
int printf(const char *format, ...);
	
#ifdef __cplusplus
}
#endif
 
#endif
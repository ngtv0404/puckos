#ifndef _STDLIB_H
#define _STDLIB_H 1
 
#include <sys/cdefs.h>
#include <stdint.h>
 
#ifdef __cplusplus
extern "C" {
#endif
 
__attribute__((__noreturn__))
void abort(void);
	
int atoi(const char *str);
char* itoa(int num, char* str, uint8_t base);
char* itoa_unsigned(uint32_t num, char* str, uint8_t base);
int rand(void);
void srand(uint32_t seed);	

#ifdef __cplusplus
}
#endif
 
#endif
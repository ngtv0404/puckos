#ifndef _MATH_H
#define _MATH_H 1
 
#include <sys/cdefs.h>
 
#include <stddef.h>
#include <stdint.h>
 
#ifdef __cplusplus
extern "C" {
#endif
 
float abs(float n);
float pow(float base, int64_t degree);
 
#ifdef __cplusplus
}
#endif
 
#endif
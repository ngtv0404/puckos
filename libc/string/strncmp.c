#include <string.h>

int strncmp(const void* str1, const void* str2, size_t n) {
	uint8_t* s1 = (uint8_t*) str1;
	uint8_t* s2 = (uint8_t*) str2;
	while(n && *s1 && (*s1 == *s2)) {
		s1++;
		s2++;
		n--;
	}
	if(n == 0) return 0;
	else return (*(uint8_t *)s1 - *(uint8_t *)s2);
}
#include <math.h>

inline float pow(float base, int64_t degree) {
	float baseOld = base;
	if(degree == 0) return 1;
	for(uint64_t i = 1; i < abs(degree); i++) base *= baseOld;
	if(degree < 0) base = 1 / base;
	return base;
}
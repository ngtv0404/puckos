#include <math.h>

inline float abs(float n) {
	if(n < 0) return -n;
	else return n;
}
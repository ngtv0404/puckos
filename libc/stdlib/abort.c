#include <stdlib.h>
#include <stdio.h>
 
__attribute__((__noreturn__))
void abort(void) {
#if defined(__is_libk)
	printf("[FATAL] abort(): kernel committed suicide\n");
#else
	printf("[DEBUG] abort(): process terminated\n");
#endif
	while(1);
	__builtin_unreachable();
}
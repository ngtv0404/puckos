/* 
	gdt32.c - 32-bit GDT library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains functions that can only be used on x86
	machines and therefore is i386-specific.
*/

#include <kernel/gdt32.h>
#include <string.h>

extern void gdt32_flush(gdt32_register *gdtr);

void gdt32_entry(uint16_t entry, uint32_t base, uint32_t limit, uint8_t access, uint8_t granularity) {
	/* base */
	gdt32_gates[entry].base0_15 = (base & 0xFFFFF);
	gdt32_gates[entry].base16_23 = (base >> 16) & 0xFF;
	gdt32_gates[entry].base24_31 = (base >> 24) & 0xFF;
	
	/* limit + flags */
	gdt32_gates[entry].limit0_15 = (limit & 0xFFFF);
	gdt32_gates[entry].limit16_19_flags = ((limit >> 16) & 0x0F);
	gdt32_gates[entry].limit16_19_flags |= (granularity & 0xF0);
	
	gdt32_gates[entry].access_byte = access; // access byte
}

void gdt32_init(void) {
	/* set up gdt register */
	gdtr.limit = (sizeof(gdt32_gate) * GDT32_ENTRIES) - 1;
	gdtr.base = (uint32_t) &gdt32_gates;
	
	memset(&gdt32_gates, 0, gdtr.limit + 1); // clear out gdt gates
	gdt32_entry(1, 0x00000000, 0xFFFFFFFF, 0x9A, 0xCF); // kernel code segment
	gdt32_entry(2, 0x00000000, 0xFFFFFFFF, 0x92, 0xCF); // kernel data segment
	gdt32_entry(3, 0x00000000, 0x000FFFFF, 0x9A, 0x8F); // 16-bit code segment
	gdt32_entry(4, 0x00000000, 0x000FFFFF, 0x92, 0x8F); // 16-bit data segment
	
	gdt32_flush((uint32_t) &gdtr);
}

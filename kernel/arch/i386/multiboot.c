/*
	multiboot.c - Multiboot header parser
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#include <kernel/multiboot.h>

extern mb_header_t *multiboot_header; // our multiboot header

uint64_t mb_get_memsize(void) {
	return ((multiboot_header->mem_lower + multiboot_header->mem_upper) * 1024);
}

void mb_get_bootdev(uint8_t *drive, uint8_t *p1, uint8_t *p2, uint8_t *p3) {
	*p3 = (uint8_t) (multiboot_header->boot_device & 0xFF);
	*p2 = (uint8_t) ((multiboot_header->boot_device >> 8) & 0xFF);
	*p1 = (uint8_t) ((multiboot_header->boot_device >> 16) & 0xFF);
	*drive = (uint8_t) ((multiboot_header->boot_device >> 24) & 0xFF);
}
/* 
	inlineasm.c - Inline Assembly library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains i386-specific code for basic I/O functions
	and therefore is i386-specific. You should only include this in
	i386-specific files.
	This file uses information from:
	 - OSDev wiki: https://wiki.osdev.org/Inline_Assembly/Examples
	 - Linux source code
*/

#include <kernel/inlineasm.h>

inline void outb(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

inline void io_wait(void) {
	asm volatile ( "outb %%al, $0x80" : : "a"(0) );
}

inline uint16_t inw(uint16_t port) {
	uint16_t ret;
	asm volatile("inw %w1,%0":"=a"(ret):"Nd"(port));
	return ret;
}

inline uint32_t inl(uint16_t port) {
	uint32_t ret;
	asm volatile("inl %w1,%0":"=a"(ret):"Nd"(port));
	return ret;
}

inline void outw(uint16_t port, uint16_t val) {
	asm volatile ("outw %w0, %w1"::"a"(val),"Nd"(port));
}

inline void outl(uint16_t port, uint32_t val) {
	asm volatile ("outl %0, %w1"::"a"(val),"Nd"(port));
}

inline void wrmsr(uint32_t msr, uint64_t val) {
	asm volatile("wrmsr"::"c"(msr),"a"((uint32_t) val & 0xFFFFFFFF),"d"(((uint32_t) val >> 32) & 0xFFFFFFFF));
}

inline uint64_t rdmsr(uint32_t msr) {
	uint32_t low, high;
	asm volatile("rdmsr":"=a"(low),"=d"(high):"c"(msr));
	return ((uint64_t) high << 32) | low;
}
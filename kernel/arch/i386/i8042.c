/*
	i8042.c - Intel 8042 "PS/2 Controller" driver
	<C> 2018 Weedboi6969@RTEA
*/

#include <kernel/i8042.h>
#include <kernel/i8259.h>
#include <kernel/inlineasm.h>
#include <stdio.h>

uint8_t i8042_get_status(void) {
	return inb(0x64);
}

void i8042_send_cmd(uint8_t command) {
	while(i8042_get_status() & 0x02); // wait until input buffer is empty
	outb(0x64, command); // send command
}

void i8042_send_data(uint8_t data) {
	while(i8042_get_status() & 0x02); // wait until input buffer is empty
	outb(0x60, data);
}

uint8_t i8042_get_data(void) {
	while(!(i8042_get_status() & 0x01)); // wait until output buffer is full
	return inb(0x60);
}

void i8042_send_port(uint8_t port, uint8_t data) {
	if(port) i8042_send_cmd(0xD4); // write to second port
	i8042_send_data(data);
}

uint8_t i8042_dualchannel = 0;
uint8_t i8042_port0 = 0, i8042_port1 = 0;
uint16_t i8042_id0 = 0xFFFF, i8042_id1 = 0xFFFF;
uint8_t i8042_scs0 = 0, i8042_scs1 = 0;

uint8_t i8042_get_scset(uint8_t port) {
	/* disable interrupt */
	asm("cli");
	pic_mask((port) ? 12 : 1);
	i8042_send_cmd(0x20); // get CCB
	uint8_t ccb = i8042_get_data();
	ccb &= (port) ? (~(1 << 1)) : (~(1 << 0));
	i8042_send_cmd(0x60); // set CCB
	i8042_send_data(ccb);
	uint8_t ret, i, j;
	for(i = 0; i < 10; i++) {
		i8042_send_port(port, 0xF0);
		uint8_t status = i8042_get_data();
		if(status == 0xFA) {
			for(j = 0; j < 10; j++) {
				i8042_send_port(port, 0x00);
				status = i8042_get_data();
				if(status == 0xFA) break;
				else printf("[ERROR] i8042_get_scset(): received unspecified status 0x%X, resending parameter\n", status);
			}
			if(j >= 9) break; // we don't care about that for now
			ret = i8042_get_data(); // get scan code set number
			break;
		} else {
			printf("[ERROR] i8042_get_scset(): received unspecified status 0x%X, resending command\n", status);
		}
	}

	/* enable interrupt */
	i8042_send_cmd(0x20); // get CCB
	ccb = i8042_get_data();
	ccb |= (port) ? (1 << 1) : (1 << 0);
	i8042_send_cmd(0x60); // set CCB
	i8042_send_data(ccb);
	pic_unmask((port) ? 12 : 1);
	asm("sti");
	
	if(i >= 9) {
		printf("[ERROR] i8042_get_scset(): sending command to device %u failed, returning 0\n", port);
		return 0;
	}
	if(j >= 9) {
		printf("[ERROR] i8042_get_scset(): sending parameter to device %u failed, returning 0\n", port);
		return 0;
	}
	
	if(port == 0) i8042_scs0 = ret;
	else i8042_scs1 = ret;
	
	return ret;
}

int8_t i8042_set_scset(uint8_t port, uint8_t set) {
	if(set == 0) return;
	/* disable interrupt */
	asm("cli");
	pic_mask((port) ? 12 : 1);
	i8042_send_cmd(0x20); // get CCB
	uint8_t ccb = i8042_get_data();
	ccb &= (port) ? (~(1 << 1)) : (~(1 << 0));
	i8042_send_cmd(0x60); // set CCB
	i8042_send_data(ccb);
	uint8_t i, j;
	for(i = 0; i < 10; i++) {
		i8042_send_port(port, 0xF0);
		uint8_t status = i8042_get_data();
		if(status == 0xFA) {
			for(j = 0; j < 10; j++) {
				i8042_send_port(port, set);
				status = i8042_get_data();
				if(status == 0xFA) break;
				else printf("[ERROR] i8042_set_scset(): received unspecified status 0x%X, resending parameter\n", status);
			}
			if(j >= 9) break; // we don't care about that for now
			break;
		} else {
			printf("[ERROR] i8042_set_scset(): received unspecified status 0x%X, resending command\n", status);
		}
	}
	
	uint8_t newsc = i8042_get_scset(port);
	if(newsc != set) {
		printf("[ERROR] i8042_set_scset(): set scancode set command didn't take effect, is the set unsupported? (set %u, got %u)\n", set, newsc);
		printf("[ERROR] i8042_set_scset(): returning with error code -2\n");
		return -2;
	}
	
	/* enable interrupt */
	i8042_send_cmd(0x20); // get CCB
	ccb = i8042_get_data();
	ccb |= (port) ? (1 << 1) : (1 << 0);
	i8042_send_cmd(0x60); // set CCB
	i8042_send_data(ccb);
	pic_unmask((port) ? 12 : 1);
	asm("sti");
	
	if(i >= 9) {
		printf("[ERROR] i8042_set_scset(): sending command to device %u failed, returning with error code -1\n", port);
		return -1;
	}
	if(j >= 9) {
		printf("[ERROR] i8042_set_scset(): sending parameter to device %u failed, returning with error code -1\n", port);
		return -1;
	}
}

int8_t i8042_init(void) {
	/* disable devices */
	i8042_send_cmd(0xAD); // disable first channel
	i8042_send_cmd(0xA7); // disable second channel. single channel controller will ignore this
	printf("[DEBUG] i8042_init(): temporarily disabled all ports\n");
	
	/* flush the output buffer */
	for(uint8_t i = 0; i < 16; i++) inb(0x60); // perform dummy reads
	printf("[DEBUG] i8042_init(): performed 16 dummy reads\n");
	
	/* modify the CCB */
	i8042_send_cmd(0x20); // get CCB
	uint8_t ccb = i8042_get_data();
	printf("[DEBUG] i8042_init(): initial CCB read, CCB = 0x%X\n", ccb);
	ccb &= 0xBC; // disable PS/2 interrupts and translation
	i8042_dualchannel = (ccb & 0x20) ? 1 : 0; // check if controller is dual channel
	if(i8042_dualchannel) printf("[INFO ] i8042_init(): controller seems to support dual channel\n");
	else printf("[INFO ] i8042_init(): controller doesn't support dual channel\n");
	i8042_send_cmd(0x60); // set CCB
	i8042_send_data(ccb);
	printf("[DEBUG] i8042_init(): set CCB to 0x%X\n", ccb);
	
	/* perform controller test */
	i8042_send_cmd(0xAA); // send test command
	if(i8042_get_data() != 0x55) {
		printf("[ERROR] i8042_init(): controller test failed, returning with error code -1\n");
		return -1;
	} else {
		printf("[DEBUG] i8042_init(): controller test performed successfully\n");
		i8042_send_cmd(0x60); // set CCB
		i8042_send_data(ccb);
		printf("[DEBUG] i8042_init(): set CCB back to its original value just in case\n", ccb);
	}
	
	/* determine if there are 2 channels */
	if(i8042_dualchannel) {
		/* only check if the controller seems to be dual channel */
		i8042_send_cmd(0xA8); // enable second channel
		printf("[DEBUG] i8042_init(): re-enabled second channel to test for dual channel support\n");
		i8042_send_cmd(0x20); // get CCB
		ccb = i8042_get_data();
		printf("[DEBUG] i8042_init(): CCB is now 0x%X\n", ccb);
		i8042_dualchannel = (ccb & 0x20) ? 0 : 1;
		if(i8042_dualchannel) {
			printf("[INFO ] i8042_init(): controller supports dual channel\n");
			i8042_send_cmd(0xA7); // disable second channel again
		}
		else printf("[INFO ] i8042_init(): controller doesn't support dual channel\n");
	}
	
	/* perform interface tests */
	i8042_send_cmd(0xAB); // test first port
	i8042_port0 = i8042_get_data();
	if(i8042_port0) {
		/* first port test failed */
		printf("[INFO ] i8042_init(): first port interface test failed with error code 0x%X\n", i8042_port0);
		i8042_port0 = 0;
	} else {
		/* first port test passed */
		printf("[INFO ] i8042_init(): first port interface test passed\n");
		i8042_port0 = 1;
	}
	if(i8042_dualchannel) {
		i8042_send_cmd(0xA9); // test second port
		i8042_port1 = i8042_get_data();
		if(i8042_port1) {
			/* second port test failed */
			printf("[INFO ] i8042_init(): second port interface test failed with error code 0x%X\n", i8042_port1);
			i8042_port1 = 0;
		} else {
			/* second port test passed */
			printf("[INFO ] i8042_init(): second port interface test passed\n");
			i8042_port1 = 1;
		}
	}
	if(!i8042_port0 && !i8042_port1) {
		/* no port is usable */
		printf("[ERROR] i8042_init(): all ports are unusable, returning with error code -2\n");
		return -2;
	}
	
	/* enable devices */
	if(i8042_port0) {
		i8042_send_cmd(0xAE); // enable first port
		printf("[DEBUG] i8042_init(): enabled first port\n");
	}
	if(i8042_port1) {
		i8042_send_cmd(0xA8); // enable second port
		printf("[DEBUG] i8042_init(): enabled second port\n");
	}
	
	/* reset devices */
	if(i8042_port0) {
		uint8_t status;
		for(uint8_t i = 0; i < 10; i++) {
			i8042_send_port(0, 0xF5); // disable scanning in case bad things happen
			status = i8042_get_data(); // receive status
			if(status == 0xFA) {
				printf("[DEBUG] i8042_init(): disabled scanning for device on first port\n");
				break;
			} else if(status == 0xFE) {
				printf("[ERROR] i8042_init(): device on first port is requesting a resend\n");
			} else {
				printf("[WARN ] i8042_init(): device on first port returns unspecified code 0x%X, resending command\n", status);
			}
		}
		if(status != 0xFA) {
			printf("[ERROR] i8042_init(): device on first port is broken\n");
			i8042_port0 = 0;
		} else {
			for(uint8_t i = 0; i < 10; i++) {
				i8042_send_port(0, 0xFF); // reset device on first port
				status = i8042_get_data();
				if(status == 0xFA) {
					printf("[DEBUG] i8042_init(): reset device on first port, waiting for self test result\n");
					status = i8042_get_data();
					if(status == 0xAA) {
						printf("[DEBUG] i8042_init(): device on first port finished self test successfully, waiting for device ID\n");
						uint32_t j;
						for(j = 0; j < 0x10000; j++) {
							if(i8042_get_status() & 0x01) {
								i8042_id0 = inb(0x60);
								if(i8042_id0 == 0xAB) {
									printf("[INFO ] i8042_init(): device on first port is an MF2 keyboard\n");
									i8042_id0 <<= 8;
									i8042_id0 |= i8042_get_data();
									printf("[DEBUG] i8042_init(): received second byte\n");
								}
								printf("[INFO ] i8042_init(): device on first port sent its ID 0x%X\n", i8042_id0);
								break;
							}
						}
						if(j >= 0xFFFF) printf("[INFO ] i8042_init(): device on first port didn't send its ID, probably a normal PS/2 keyboard\n");
						for(j = 0; j < 10; j++) {
							i8042_send_port(0, 0xF4); // re-enable scanning
							status = i8042_get_data();
							if(status == 0xFA) {
								printf("[DEBUG] i8042_init(): enabled scanning for device on first port\n");
								break;
							} else if(status == 0xFE) {
								printf("[ERROR] i8042_init(): device on first port is requesting a resend\n");
							} else {
								printf("[WARN ] i8042_init(): device on first port returns unspecified code 0x%X, resending command\n", status);
							}
						}
						if(status != 0xFA) {
							printf("[ERROR] i8042_init(): device on first port is broken\n");
							i8042_port0 = 0;
						}
					} else {
						printf("[ERROR] i8042_init(): device on first port failed self test (status = 0x%X)\n", status);
						i8042_port0 = 0;
					}
					break;
				} else if(status == 0xFE) {
					printf("[ERROR] i8042_init(): device on first port is requesting a resend\n");
				} else if(status == 0xFC || status == 0xFD) {
					printf("[ERROR] i8042_init(): failed to reset device on first port (status = 0x%X)\n", status);
					i8042_port0 = 0;
				}
			}
		}
	}
	if(i8042_port1) {
		uint8_t status;
		for(uint8_t i = 0; i < 10; i++) {
			i8042_send_port(1, 0xF5); // disable scanning in case bad things happen
			status = i8042_get_data(); // receive status
			if(status == 0xFA) {
				printf("[DEBUG] i8042_init(): disabled scanning for device on second port\n");
				break;
			} else if(status == 0xFE) {
				printf("[ERROR] i8042_init(): device on second port is requesting a resend\n");
			} else {
				printf("[WARN ] i8042_init(): device on second port returns unspecified code 0x%X, resending command\n", status);
			}
		}
		if(status != 0xFA) {
			printf("[ERROR] i8042_init(): device on second port is broken\n");
			i8042_port1 = 0;
		} else {
			for(uint8_t i = 0; i < 10; i++) {
				i8042_send_port(1, 0xFF); // reset device on second port
				status = i8042_get_data();
				if(status == 0xFA) {
					printf("[DEBUG] i8042_init(): reset device on second port, waiting for self test result\n");
					status = i8042_get_data();
					if(status == 0xAA) {
						printf("[DEBUG] i8042_init(): device on second port finished self test successfully, waiting for device ID\n");
						uint32_t j;
						for(j = 0; j < 0x10000; j++) {
							if(i8042_get_status() & 0x01) {
								i8042_id1 = inb(0x60);
								if(i8042_id0 == 0xAB) {
									printf("[INFO ] i8042_init(): device on second port is an MF2 keyboard\n");
									i8042_id0 <<= 8;
									i8042_id0 |= i8042_get_data();
									printf("[DEBUG] i8042_init(): received second byte\n");
								}
								printf("[INFO ] i8042_init(): device on second port sent its ID 0x%X\n", i8042_id1);
								break;
							}
						}
						if(j >= 0xFFFF) printf("[INFO ] i8042_init(): device on second port didn't send its ID, probably a normal PS/2 keyboard\n");
						for(j = 0; j < 10; j++) {
							i8042_send_port(1, 0xF4); // re-enable scanning
							status = i8042_get_data();
							if(status == 0xFA) {
								printf("[DEBUG] i8042_init(): enabled scanning for device on second port\n");
								break;
							} else if(status == 0xFE) {
								printf("[ERROR] i8042_init(): device on second port is requesting a resend\n");
							} else {
								printf("[WARN ] i8042_init(): device on second port returns unspecified code 0x%X, resending command\n", status);
							}
						}
						if(status != 0xFA) {
							printf("[ERROR] i8042_init(): device on second port is broken\n");
							i8042_port1 = 0;
						}
					} else {
						printf("[ERROR] i8042_init(): device on second port failed self test (status = 0x%X)\n", status);
						i8042_port1 = 0;
					}
					break;
				} else if(status == 0xFE) {
					printf("[ERROR] i8042_init(): device on second port is requesting a resend\n");
				} else if(status == 0xFC || status == 0xFD) {
					printf("[ERROR] i8042_init(): failed to reset device on second port (status = 0x%X)\n", status);
					i8042_port1 = 0;
				}
			}
		}
	}
	
	if(!i8042_port0 && !i8042_port1) {
		/* no port is usable */
		printf("[ERROR] i8042_init(): all ports are unusable, returning with error code -2\n");
		return -2;
	}
	
	/* enable interrupts */
	if(i8042_port0) {
		i8042_send_cmd(0x20); // get CCB
		ccb = i8042_get_data();
		ccb |= 0x01; // enable first port interrupt
		i8042_send_cmd(0x60); // set CCB
		i8042_send_data(ccb);
		printf("[DEBUG] i8042_init(): enabled IRQ on first port (CCB = 0x%X)\n", ccb);
		
	}
	if(i8042_port1) {
		i8042_send_cmd(0x20); // get CCB
		ccb = i8042_get_data();
		ccb |= 0x02; // enable second port interrupt
		i8042_send_cmd(0x60); // set CCB
		i8042_send_data(ccb);
		printf("[DEBUG] i8042_init(): enabled IRQ on second port (CCB = 0x%X)\n", ccb);
	}
	
	/* mask IRQs */
	pic_mask(1);
	pic_mask(12);
	
	printf("[INFO ] i8042_init(): controller initialized successfully\n");
	printf("[INFO ] i8042_init(): dual channel = %u, port 0 available = %u, port 1 available = %u, port 0 ID = 0x%X, port 1 ID = 0x%X\n", i8042_dualchannel, i8042_port0, i8042_port1, i8042_id0, i8042_id1);
	if(i8042_port0 && (i8042_id0 == 0xFFFF || (i8042_id0 & 0xFF00) == 0xAB00))
		printf("[INFO ] i8042_init(): device on port 0 uses scan code set %u\n", i8042_get_scset(0));
	if(i8042_port1 && (i8042_id1 == 0xFFFF || (i8042_id1 & 0xFF00) == 0xAB00))
		printf("[INFO ] i8042_init(): device on port 1 uses scan code set %u\n", i8042_get_scset(1));
	return 0;
}
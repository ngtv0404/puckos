/* 
	uart.c - UART/COM library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains i386-specific code for UART/COM communication
	and therefore is i386-specific. You should only include this in
	i386-specific files.
	This file uses information from OSDev wiki:
	https://wiki.osdev.org/Serial_Ports
*/

#include <kernel/uart.h>
#include <kernel/bda.h>
#include <kernel/inlineasm.h>

void uart_init(uint8_t port, uint32_t baud) {
	/* sanity check: check if baud rate is not over 115200, is a divisor of 115200, and port is not over 3 (COM4) */
	if((baud > 115200) || (115200 % baud) || (port > 3)) return;
	
	/* get I/O port base */
	uint16_t base = bda->com_base[port];
	if(base == 0) return; // the initalizing port is not enabled
	
	/* configure port */
	uint16_t divisor = 115200 / baud; // baud divisor = 115200 / baud
	outb(base + 1, 0x00); // disable all interrupts
	outb(base + 3, 0x80); // enable DLAB (set baud rate divisor)
	outb(base + 0, (uint8_t) divisor); // set low byte
	outb(base + 1, (uint8_t) (divisor >> 8)); // set high byte
	outb(base + 3, 0x03); // 8 bits, no parity, 1 stop bit
	outb(base + 2, 0xC7); // enable FIFO, clear them. with 14-byte threshold
}

uint8_t uart_received(uint8_t port) {
	/* get I/O port base */
	uint16_t base = bda->com_base[port];
	if(base == 0) return;
	return (inb(base + 3) & 1);
}

uint8_t uart_getc(uint8_t port) {
	while(!uart_received(port)); // wait until receive buffer is full
	
	/* get I/O port base */
	uint16_t base = bda->com_base[port];
	if(base == 0) return;
	return inb(base); // read from receive buffer and return
}

uint8_t uart_empty(uint8_t port) {
	/* get I/O port base */
	uint16_t base = bda->com_base[port];
	if(base == 0) return;
	return (inb(base + 5) & 0x20);
}

void uart_putc(uint8_t port, uint8_t c) {
	while(!uart_empty(port)); // wait until transmit buffer is empty
	
	/* get I/O port base */
	uint16_t base = bda->com_base[port];
	if(base == 0) return;
	outb(base, c); // write to transmit buffer
}

void uart_puts(uint8_t port, const char *s) {
	for(uint32_t i = 0; s[i] != 0; i++) uart_putc(port, s[i]);
}
.global pic1_isr
pic1_isr:
push %eax
mov $0x20, %al
out %al, $0x20
pop %eax
iret

.global pic2_isr
pic2_isr:
push %eax
mov $0x20, %al
out %al, $0xA0
out %al, $0x20
pop %eax
iret
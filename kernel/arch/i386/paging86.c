/*
	paging.c - x86 paging library
	Code from http://www.jamesmolloy.co.uk/tutorial_html/9.-Multitasking.html
*/

#include <kernel/paging86.h>
#include <kernel/kheap.h>
#include <kernel/multiboot.h>
#include <kernel/uart.h>
#include <string.h>

/* page directory pointers */
paging86_directory_t *kernel_directory = NULL; // pointer to kernel's page directory
paging86_directory_t *current_directory = NULL; // pointer to current page directory

/* frame bitset information */
uint32_t *frames;
uint32_t nframes;
uint64_t mem_end_page;

extern uint32_t placement_address; // defined in kheap.c

/* frame bitset manipulation functions */
#define INDEX_FROM_BIT(a)				(a / 32)
#define OFFSET_FROM_BIT(a)				(a % 32)

static void frame_set(uint32_t frame_addr) {
	uint32_t frame = frame_addr / 0x1000;
	uint32_t index = INDEX_FROM_BIT(frame);
	uint32_t offset = OFFSET_FROM_BIT(frame);
	frames[index] |= (1 << offset);
}

static void frame_clear(uint32_t frame_addr) {
	uint32_t frame = frame_addr / 0x1000;
	uint32_t index = INDEX_FROM_BIT(frame);
	uint32_t offset = OFFSET_FROM_BIT(frame);
	frames[index] &= ~(1 << offset);
}

static uint32_t frame_test(uint32_t frame_addr) {
	uint32_t frame = frame_addr / 0x1000;
	uint32_t index = INDEX_FROM_BIT(frame);
	uint32_t offset = OFFSET_FROM_BIT(frame);
	return (frames[index] & (1 << offset));
}

static uint32_t frame_first(void) {
	for(uint32_t i = 0; i < INDEX_FROM_BIT(nframes); i++) {
		if(frames[i] != 0xFFFFFFFF) {
			for(uint32_t j = 0; j < 32; j++) {
				if(!(frames[i] & (1 << j))) return i * 32 + j;
			}
		}
	}
	return (uint32_t) -1;
}

/* frame management functions */
void frame86_alloc(paging86_page_t *page, uint8_t is_kernel, uint8_t is_writable) {
	if(page->frame != 0) return; // frame already allocated
	uint32_t idx = frame_first();
	if(idx == (uint32_t) -1) {
		// no free frames, enter an infinite loop
		// TODO: write a proper kernel panic function
		while(1);
	}
	frame_set(idx * 0x1000);
	page->present = 1;
	page->rw = (is_writable) ? 1 : 0;
	page->user = (is_kernel) ? 0 : 1;
	page->frame = idx;
}

void frame86_free(paging86_page_t *page) {
	uint32_t frame = page->frame;
	if(!frame) return; // frame already freed
	frame_clear(frame);
	page->frame = 0x00000000;
}

/* paging functions */
paging86_page_t *paging86_getpage(uint32_t address, uint8_t make, paging86_directory_t *dir) {
	address /= 0x1000;
	uint32_t table_idx = address / 1024;
	if(dir->tables[table_idx]) return &dir->tables[table_idx]->pages[address % 1024]; // page already made, return straight away
	if(make) {
		// make a page
		uint32_t tmp;
		dir->tables[table_idx] = (paging86_table_t*) kmalloc_ap(sizeof(paging86_table_t), &tmp);
		// hexstring(tmp);
		memset(dir->tables[table_idx], 0, sizeof(paging86_table_t));
		dir->tables_phys[table_idx] = tmp | 0x07;
		return &dir->tables[table_idx]->pages[address % 1024];
	}
	return 0; // page unavailable
}

void paging86_switchdir(paging86_directory_t *dir) {
	current_directory = dir;
	asm volatile("mov %0, %%cr3" :: "r"(dir->phys_addr));
	uint32_t cr0;
	asm volatile("mov %%cr0, %0" : "=r"(cr0));
	cr0 |= 0x80000001;
	asm volatile("mov %0, %%cr0" :: "r"(cr0));
}


void paging86_map(uint32_t paddr, uint32_t vaddr, uint32_t size, uint8_t is_kernel, uint8_t is_writable, paging86_directory_t *dir) {
	paddr &= 0xFFFFF000;
	vaddr &= 0xFFFFF000;
	for(uint32_t i = 0; i < size; i += 0x1000) {
		paging86_page_t *page = paging86_getpage(vaddr, 1, dir);
		if(paddr < mem_end_page) frame_set(paddr);
		page->present = 1;
		page->rw = (is_writable) ? 1 : 0;
		page->user = (is_kernel) ? 0 : 1;
		page->frame = paddr >> 12;
		paddr += 0x1000;
		vaddr += 0x1000;
	}
}

void paging86_unmap(uint32_t vaddr, uint32_t size, paging86_directory_t *dir) {
	vaddr &= 0xFFFFF000;
	for(uint32_t i = 0; i < size; i += 0x1000) {
		paging86_page_t *page = paging86_getpage(vaddr, 0, dir);
		if(!page) return;
		page->present = 0;
		if(page->frame < mem_end_page) frame_clear(page->frame);
		page->frame = 0;
		vaddr += 0x1000;
	}
}

extern void copy_page_physical(uint32_t src, uint32_t dest);

static paging86_table_t *paging86_clone_table(paging86_table_t *src, uint32_t *phys_addr) {
	paging86_table_t *table = (paging86_table_t*) kmalloc_ap(sizeof(paging86_table_t), phys_addr);
	memset(table, 0, sizeof(paging86_directory_t));
	for(uint16_t i = 0; i < 1024; i++) {
		if(src->pages[i].frame) {
			frame86_alloc(&table->pages[i], 0, 0);
			if(src->pages[i].present) table->pages[i].present = 1;
			if(src->pages[i].rw) table->pages[i].rw = 1;
			if(src->pages[i].user) table->pages[i].user = 1;
			if(src->pages[i].accessed) table->pages[i].accessed = 1;
			if(src->pages[i].dirty) table->pages[i].dirty = 1;
			copy_page_physical(src->pages[i].frame * 0x1000, table->pages[i].frame * 0x1000);
		}
	}
	return table;
}

paging86_directory_t *paging86_clone_directory(paging86_directory_t *src) {
	uint32_t phys;
	paging86_directory_t *dir = (paging86_directory_t*) kmalloc_ap(sizeof(paging86_directory_t), &phys);
	memset(dir, 0, sizeof(paging86_directory_t));
	uint32_t offset = (uint32_t) dir->tables_phys - (uint32_t) dir;
	dir->phys_addr = phys + offset;
	for(uint16_t i = 0; i < 1024; i++) {
		if(!src->tables[i]) continue;
		if(kernel_directory->tables[i] == src->tables[i]) {
			dir->tables[i] = src->tables[i];
			dir->tables_phys[i] = src->tables_phys[i];
		} else {
			uint32_t t;
			dir->tables[i] = paging86_clone_table(src->tables[i], &t);
			dir->tables_phys[i] = t | 0x07;
		}
	}
	return dir;
}

void paging86_init(void) {
	mem_end_page = mb_get_memsize(); // get physical memory size
	uint32_t i;
	
	nframes = mem_end_page / 0x1000;
	frames = (uint32_t*) kmalloc(INDEX_FROM_BIT(nframes));
	memset(frames, 0, INDEX_FROM_BIT(nframes));
	
	kernel_directory = (paging86_directory_t*) kmalloc_a(sizeof(paging86_directory_t));
	memset(kernel_directory, 0, sizeof(paging86_directory_t));
	kernel_directory->phys_addr = (uint32_t) kernel_directory->tables_phys;
	
	for(i = KHEAP_START; i < KHEAP_START + KHEAP_INITIAL_SIZE; i += 0x1000)
		paging86_getpage(i, 1, kernel_directory);
	
	i = 0;
	while(i < placement_address + 0x1000) {
		frame86_alloc(paging86_getpage(i, 1, kernel_directory), 0, 0);
		i += 0x1000;
	}
	
	for(i = KHEAP_START; i < KHEAP_START + KHEAP_INITIAL_SIZE; i += 0x1000)
		frame86_alloc(paging86_getpage(i, 1, kernel_directory), 0, 0);

	paging86_switchdir(kernel_directory);

	kheap = create_heap(KHEAP_START, KHEAP_START + KHEAP_INITIAL_SIZE, 0xCFFFF000, 0, 0);
	
	current_directory = paging86_clone_directory(kernel_directory);
    paging86_switchdir(current_directory);
}

void paging86_enable(void) {
	uint32_t cr0;
	asm volatile("mov %%cr0, %0" : "=r"(cr0));
	cr0 |= 0x80000000;
	asm volatile("mov %0, %%cr0" :: "r"(cr0));
}

void paging86_disable(void) {
	uint32_t cr0;
	asm volatile("mov %%cr0, %0" : "=r"(cr0));
	cr0 &= 0x7FFFFFFF;
	asm volatile("mov %0, %%cr0" :: "r"(cr0));
}
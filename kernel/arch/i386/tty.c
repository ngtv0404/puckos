/*
	tty.c - TTY emulator piggybacking from video.c
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#include <kernel/tty.h>
#include <kernel/video.h>

uint16_t tty_x, tty_y;
uint16_t tty_width, tty_height;
uint32_t tty_bgcolor, tty_fgcolor;
uint8_t tty_size;

void tty_setbg(uint32_t color) {
	tty_bgcolor = color;
}

void tty_setfg(uint32_t color) {
	tty_fgcolor = color;
}

void tty_init(void) {
	tty_x = 0; tty_y = 0;
	tty_size = 1;
	tty_width = video_width / (8 * tty_size);
	tty_height = video_height / (8 * tty_size);
}

void tty_clear(void) {
	video_fill(tty_bgcolor);
}

void tty_setxy(uint16_t x, uint16_t y) {
	tty_x = x;
	tty_y = y;
}

void tty_getxy(uint16_t *x, uint16_t *y) {
	*x = tty_x;
	*y = tty_y;
}

void tty_putc(uint8_t c) {
	if(c == '\n') {
		tty_x = 0;
		tty_y++;
		if(tty_y >= tty_height) {
			tty_y = tty_height - 1;
			video_scrollup(8, tty_bgcolor);
		}
	} else if(c == '\t') {
		tty_x += 4 - (tty_x % 4);
		if(tty_x >= tty_width) {
			tty_x = 0;
			tty_y++;
		}
		if(tty_y >= tty_height) {
			tty_y = tty_height - 1;
			video_scrollup(8, tty_bgcolor);
		}
	} else if(c == '\b') {
		if(tty_x > 0) tty_x--;
		else {
			if(tty_y > 0) {
				tty_x = tty_width - 1;
				tty_y--;
			}
		}
	} else {
		video_putc(tty_x * (8 * tty_size), tty_y * (8 * tty_size), c, tty_size, tty_fgcolor);
		tty_x++;
		if(tty_x >= tty_width) {
			tty_x = 0;
			tty_y++;
		}
		if(tty_y >= tty_height) {
			tty_y = tty_height - tty_size;
			video_scrollup(8, tty_bgcolor);
		}
	}
}

void tty_puts(const char *s) {
	for(uint32_t i = 0; s[i] != 0; i++) tty_putc(s[i]);
}
/*
	i8259.h - Intel 8259 Programmable Interrupt Controller driver
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#ifndef KERNEL_I8259_H
#define KERNEL_I8259_H

#include <stddef.h>
#include <stdint.h>

void pic_eoi(uint8_t irq);
void pic_init(uint8_t offset1, uint8_t offset2);
void pic_mask(uint8_t irq);
void pic_unmask(uint8_t irq);

#endif
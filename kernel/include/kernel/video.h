/*
	video.h - Generic video driver
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#ifndef KERNEL_VIDEO_H
#define KERNEL_VIDEO_H

#include <stddef.h>
#include <stdint.h>

extern uint32_t video_width;
extern uint32_t video_height;
extern uint8_t *video_frame;
extern uint8_t *video_font;

int8_t video_init(uint16_t width, uint16_t height);
void video_fill(uint32_t color);
uint32_t rgb(uint8_t r, uint8_t g, uint8_t b);
void video_putpixel(uint16_t x, uint16_t y, uint32_t color);
void video_drawline(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint32_t color);
void video_drawrect(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint32_t color);
void video_fillrect(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint32_t color);
void video_drawbitmap(uint16_t x, uint16_t y, uint8_t *bitmap, uint16_t w, uint16_t h);
void video_setxy(uint16_t x, uint16_t y);
void video_getxy(uint16_t *x, uint16_t *y);
void video_setoffset(uint16_t x, uint16_t y);
void video_getoffset(uint16_t *x, uint16_t *y);
void video_putc(uint16_t x, uint16_t y, uint16_t c, uint8_t size, uint32_t color);
void video_puts(uint16_t x, uint16_t y, const uint8_t *s, uint8_t size, uint32_t color);
void video_scrollup(uint32_t lines, uint32_t bg);

#endif
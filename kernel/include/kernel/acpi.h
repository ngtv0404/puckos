/*
	acpi.h - ACPI driver
	<C> Copyright 2018 Weedboi6969@RTEA
*/

#ifndef KERNEL_ACPI_H
#define KERNEL_ACPI_H

#include <stddef.h>
#include <stdint.h>

typedef struct __attribute__ ((packed)) {
	/* RSDP */
	uint8_t signature[8]; // should be "RSD PTR "
	uint8_t checksum;
	uint8_t oem_id[6];
	uint8_t revision; // 0 indicates ACPI 1.0, any other values indicate ACPI 2.0+
	uint32_t rsdt_address;
	/* valid in ACPI version 2.0+ */
	uint32_t length;
	uint64_t xsdt_address; // should be treated in almost the same way as rsdt_address
	uint8_t extended_checksum;
	uint8_t reserved[3];
} acpi_rsdp_t;

struct acpi_sdt_header {
	/* ACPI SDT header */
	uint8_t signature[4]; // indicates what the SDT is for
	uint32_t length; // including the header
	uint8_t revision;
	uint8_t checksum;
	uint8_t oem_id[6];
	uint8_t oem_table_id[8];
	uint32_t oem_revision;
	uint32_t creator_id;
	uint32_t creator_rev;
} __attribute__ ((packed));

typedef struct __attribute__ ((packed)) {
	/* RSDT, use this if you are using rsdt_address */
	struct acpi_sdt_header header;
	uint32_t sdt_pointer[];
} acpi_rsdt_t;

typedef struct __attribute__ ((packed)) {
	/* XSDT, use this if you are using xsdt_address */
	struct acpi_sdt_header header;
	uint32_t sdt_pointer[];
} acpi_xsdt_t;

typedef struct __attribute__ ((packed)) {
	/* Generic Address Structure */
	uint8_t addr_space;
	uint8_t bit_width;
	uint8_t bit_offset;
	uint8_t access_size;
	uint64_t address;
} acpi_gas_t;

typedef struct __attribute__ ((packed)) {
	/* Fixed ACPI Description Table */
	struct acpi_sdt_header header;
	uint32_t firmware_ctrl; // pointer to FACS
	uint32_t dsdt; // pointer to DSDT
	
	uint8_t reserved;
	
	/*
		Preferred Power Management Profile:
		0 = Unspecified
		1 = Desktop
		2 = Mobile
		3 = Workstation
		4 = Enterprise Server
		5 = SOHO Server
		6 = Appliance PC
		7 = Performance Server
	*/
	uint8_t preferred_pmprofile;
	uint16_t sci_interrupt; // SCI IRQ number when working with the PIC, will be useful later on
	uint32_t smi_cmdport; // port for disabling/enabling ACPI
	uint8_t acpi_enable; // output to smi_cmdport to enable ACPI
	uint8_t acpi_disable; // output to smi_cmdport to disable ACPI
	uint8_t s4bios_req;
	uint8_t pstate_ctrl;
	uint32_t pm1a_event_blk;
	uint32_t pm1b_event_blk;
	uint32_t pm1a_ctrl_blk;
	uint32_t pm1b_ctrl_blk;
	uint32_t pm2_ctrl_blk;
	uint32_t pm_timer_blk;
	uint32_t gpe0_blk;
	uint32_t gpe1_blk;
	uint8_t pm1_event_len;
	uint8_t pm1_ctrl_len;
	uint8_t pm2_ctrl_len;
	uint8_t pm_timer_len;
	uint8_t gpe0_len;
	uint8_t gpe1_len;
	uint8_t gpe1_base;
	uint8_t cstate_ctrl;
	uint16_t worst_c2_latency;
	uint16_t worst_c3_latency;
	uint16_t flush_size;
	uint16_t flush_stride;
	uint8_t duty_offset;
	uint8_t duty_width;
	uint8_t day_alarm;
	uint8_t month_alarm;
	uint8_t century;
	
	uint16_t boot_arch_flags; // used since ACPI 2.0
	
	uint8_t reserved2;
	uint32_t flags;
	
	acpi_gas_t reset_reg;
	
	uint8_t reset_val;
	uint8_t reserved3[3];
	
	uint64_t x_firmware_ctrl; // 64-bit pointer to FACS, only used when FACS is located beyond 4GB
	uint64_t x_dsdt; // 64-bit pointer to DSDT, only used when DSDT is located beyond 4GB
	
	acpi_gas_t x_pm1a_event_blk;
	acpi_gas_t x_pm1b_event_blk;
	acpi_gas_t x_pm1a_ctrl_blk;
	acpi_gas_t x_pm1b_ctrl_blk;
	acpi_gas_t x_pm2_ctrl_blk;
	acpi_gas_t x_pm_timer_blk;
	acpi_gas_t x_gpe0_blk;
	acpi_gas_t x_gpe1_blk;
} acpi_fadt_t;

int8_t acpi_init(void);
int8_t acpi_shutdown(void);

#endif
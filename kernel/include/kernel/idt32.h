/* 
	idt32.h - 32-bit IDT library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains functions that can only be used on x86
	machines and therefore is i386-specific.
*/

#ifndef KERNEL_IDT32_H
#define KERNEL_IDT32_H

#include <stddef.h>
#include <stdint.h>

#pragma pack(push)
#pragma pack(1)

typedef struct __attribute__ ((packed)) {
	uint16_t limit;
	uint32_t base;
} idt32_register;

typedef struct __attribute__ ((packed)) {
	uint16_t offset_low;
	uint16_t selector;
	uint8_t zero;
	uint8_t type_attribs;
	uint16_t offset_high;
} idt32_gate;

#pragma pack(pop)

#define IDT32_ENTRIES				256
idt32_gate idt32_gates[IDT32_ENTRIES];
idt32_register idtr;

void idt32_entry(uint8_t vector, uint16_t selector, uint32_t handler, uint8_t attribs);
void idt32_init(void);

#endif
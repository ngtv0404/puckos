/*
	paging86.h - x86 paging library
	Code from http://www.jamesmolloy.co.uk/tutorial_html/6.-Paging.html
*/

#ifndef KERNEL_PAGING86_H
#define KERNEL_PAGING86_H

#include <stddef.h>
#include <stdint.h>

typedef struct __attribute__ ((packed)) {
	uint8_t present		: 1; // set if page is present
	uint8_t rw			: 1; // set if page is writable
	uint8_t user		: 1; // set if page is available to usermode
	uint8_t accessed	: 1; // set if page has been accessed
	uint8_t dirty		: 1; // set if page has been written to
	uint8_t unused		: 7; // unused and reserved bits
	uint32_t frame		: 20; // frame address (>> 12)
} paging86_page_t;

typedef struct __attribute__ ((packed)) {
	paging86_page_t pages[1024];
} paging86_table_t;

typedef struct __attribute__ ((packed)) {
	paging86_table_t *tables[1024]; // pointers to page tables
	uint32_t tables_phys[1024]; // physical pointers to page tables
	uint32_t phys_addr; // physical address of tables_phys
} paging86_directory_t;

extern paging86_directory_t *kernel_directory; // pointer to kernel's page directory
extern paging86_directory_t *current_directory; // pointer to current page directory

void frame86_alloc(paging86_page_t *page, uint8_t is_kernel, uint8_t is_writable);
void frame86_free(paging86_page_t *page);
paging86_page_t *paging86_getpage(uint32_t address, uint8_t make, paging86_directory_t *dir);
void paging86_switchdir(paging86_directory_t *dir);
void paging86_init(void);
void paging86_map(uint32_t paddr, uint32_t vaddr, uint32_t size, uint8_t is_kernel, uint8_t is_writable, paging86_directory_t *dir);
void paging86_unmap(uint32_t vaddr, uint32_t size, paging86_directory_t *dir);
paging86_directory_t *paging86_clone_directory(paging86_directory_t *src);
void paging86_enable(void);
void paging86_disable(void);

#endif
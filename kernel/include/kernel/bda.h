/* 
	bda.h - Support file for BIOS Data Area
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains structure for the BIOS Data Area and
	therefore is i386-specific. You should only include this in
	i386-specific files.
	This file was made in help by stanislavs.org's BDA memory map:
	http://stanislavs.org/helppc/bios_data_area.html
*/

#ifndef KERNEL_BDA_H
#define KERNEL_BDA_H

#include <stddef.h>
#include <stdint.h>

/* BIOS Data Area struct, pretty boring and filled with unneeded garbage */
struct bda_info {
	uint16_t com_base[4]; // COM1->COM4 IO port base
	uint16_t lpt_base[3]; // LPT1->LPT3 IO port base
	uint16_t ebda; // EBDA address >> 4, TODO: shift left this by 4 bits
	/* equipment list flags */
	uint8_t int11_al; // value in INT 11h register AL
	uint8_t int11_ah; // value in INT 11h register AH
	uint8_t pcjr_kberrors; // PCjr-specific: infrared keyboard link error count
	uint16_t memory_size_kb; // memory size in kbytes (INT 12h related)
	uint8_t reserved;
	uint8_t ps2_bcflags; // PS/2-specific (?): BIOS control flags
	/* keyboard flag bytes */
	uint8_t kb_flag0; // byte 0
	uint8_t kb_flag1; // byte 1
	uint8_t alt_kpentry; // storage for alternate keypad entry (?)
	uint16_t kbuff_head; // offset from 0x400 (0040:0000) to keyboard buffer head
	uint16_t kbuff_tail; // offset from 0x400 (0040:0000) to keyboard buffer tail
	uint8_t kbuff[32]; // keyboard buffer
	uint8_t recalib_status; // drive recalibration status
	uint8_t fdmotor_status; // diskette motor status
	uint8_t shutoff_counter; // motor shutoff counter (decremented by INT 8h)
	uint8_t last_fdop_status; // last diskette operation status
	uint8_t nec_fdstatus[7]; // NEC-specific: diskette controller status
	uint8_t video_mode; // current video mode
	uint16_t screen_columns; // number of screen columns
	uint16_t regenbuf_size; // curent video regen buffer size in bytes (?)
	uint16_t regenbuf_offset; // offset of current video page in video regen buffer
	uint16_t page_cursors; // cursor position of pages 1->8
	/* cursor scanline information - don't modify, maybe it's just a real mode thing */
	uint8_t cursor_bot_scanline; // cursor bottom scanline
	uint8_t cursor_top_scanline; // cursor top scanline
	uint8_t active_page; // active display page
	uint16_t crt_base; // base port address for active 6845 CRT controller. 3B4h = monochrome, 3D4h = color
	uint8_t crt_mcreg; // 6845 CRT mode control regiter value (port 3x8h)
	uint8_t cga_cpmask; // CGA current color palette mask setting (port 3D9h)
	/*
		These are 5 bytes that are pretty confusing. It can be used in one of these ways:
		 - Type 1: uint32_t pmode_ret_csip; // 286-specific: CS:IP for 286 return from protected mode
		           uint8_t reserved;
		 - Type 2: uint32_t shutdown_sssp; // temporary storage for SS:SP during shutdown (possibly for ACPI shutdown, TODO: research)
		           uint8_t reserved;
		 - Type 3: uint32_t day_counter; // specific to all IBM machines after AT: day counter
		           uint8_t reserved;
		 - Type 4: uint32_t ps2_reset; // PS/2-specific: pointer to reset code with memory preserved
		           uint8_t reserved;
		 - Type 5: uint8_t ctctrl[5]; // specific to IBM machines before AT: cassette tape control
	*/
	uint8_t unknown[5];
	uint32_t daily_tcntr; // daily timer counter, equal to 0 at midnight; incremented by INT 8h, read/set by INT 1Ah
	uint8_t rollover_flag; // clock rollover flag, set when daily_tnctr exceeds 24hrs
	uint8_t break_flag; // BIOS break flag, bit 7 is set if Ctrl-Break was hit, set by INT 9h
	uint16_t softrst_flag; // soft reset flag via Ctrl-Alt-Del or JMP FFFF:0000
	uint8_t last_hdop_status; // last hard disk operation status
	uint8_t attached_hds; // number of attached hard disks
	uint8_t xt_drvctrl; // XT-specific: fixed disk drive control byte
	uint8_t fda_offset; // port offset to current fixed disk adapter
	uint8_t lpt_timeout[4]; // time-out value for LPT1->LPT3 and LPT4 except PS/2
	uint8_t com_timeout[4]; // time-out value for COM1->COM4
	uint16_t kbuff_start; // keyboard buffer start offset (seg=40h)
	uint16_t kbuff_end; // keyboard buffer end offset (seg=40h)
	uint8_t screen_rows; // rows on the screen (EGA+)
	uint16_t cmatrix_height; // point height of character matrix (EGA+), on PCjr: character to be repeated if the typematic repeat key takes effect
	uint8_t typematic_idelay; // PCjr-specific: initial delay before repeat key action begins
	uint8_t fn_number; // PCjr-specific: current Fn function key number; video mode options (EGA+)
	uint8_t kbstat3; // PCjr-specific: third keyboard status byte; EGA feature bit switches, emulated on VGA
	uint8_t vdda; // video display data area (MCGA and VGA)
	uint8_t dcc_index; // Display Combination Code table index (EGA+)
	uint8_t last_fd_datarate; // last diskette datarate selected
	uint8_t hdstat; // hard disk status returned by controller
	uint8_t hderr; // hard disk error returned by controller
	uint8_t hdicflag; // hard disk interrupt control flag
	uint8_t combo_hdfd; // combo hard/floppy disk card when bit 0 set
	uint8_t media_state[4]; // drive 0->3 media state
	uint8_t track_d0; // track currently seeked to on drive 0
	uint8_t track_d1; // track currently seeked to on drive 1
	uint8_t kb_mode; // keyboard mode/type
	uint8_t kb_led; // keyboard LED flags
	uint32_t usrwait_flag; // pointer to user wait complete flag
	uint32_t usrwait_timeout; // user wait timeout value in microseconds
	uint8_t rtc_waitflag; // RTC wait function flag
	uint8_t lana_chflags; // LANA DMA channel flags
	uint8_t lana_status[2]; // status of LANA 0, 1
	uint32_t hd_ivt; // saved hard disk interrupt vector
	uint32_t bios_vsopt_addr; // BIOS Video Save/Override Pointer Table address
	uint8_t reserved2[8];
	uint8_t kb_nmiflags; // Convertible-specific: keyboard NMI control flags
	uint32_t kb_bpflags; // Convertible-specific: keyboard break pending flags
	uint8_t p60_queue; // Convertible-specific: port 60 single byte queue
	uint8_t last_scancode; // Convertible-specific: scan code of last key
	uint8_t nmibuff_head; // Convertible-specific: NMI buffer head pointer
	uint8_t nmibuff_tail; // Convertible-specific: NMI buffer tail pointer
	uint8_t nmi_scbuff[16]; // Convertible-specific: NMI scan code buffer
	uint16_t daycntr_cnv; // Convertible-specific: day counter
	uint8_t iaca[16]; // Intra-Applications Communications Area
} __attribute__((packed)) *bda = 0x400;

#endif
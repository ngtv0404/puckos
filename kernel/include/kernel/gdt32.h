/* 
	gdt32.h - 32-bit GDT library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains functions that can only be used on x86
	machines and therefore is i386-specific.
*/

#ifndef KERNEL_GDT32_H
#define KERNEL_GDT32_H

#include <stddef.h>
#include <stdint.h>

#pragma pack(push)
#pragma pack(1)

typedef struct __attribute__ ((packed)) {
	uint16_t limit;
	uint32_t base;
} gdt32_register;

typedef struct __attribute__ ((packed)) {
	uint16_t limit0_15;
	uint16_t base0_15;
	uint8_t base16_23;
	uint8_t access_byte;
	uint8_t limit16_19_flags;
	uint8_t base24_31;
} gdt32_gate;

#pragma pack(pop)

#define GDT32_ENTRIES				8192
gdt32_gate gdt32_gates[GDT32_ENTRIES];
gdt32_register gdtr;

void gdt32_entry(uint16_t entry, uint32_t base, uint32_t limit, uint8_t access, uint8_t granularity);
void gdt32_init(void);

#endif
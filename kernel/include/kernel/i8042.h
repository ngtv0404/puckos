/*
	i8042.h - Intel 8042 "PS/2 Controller" driver
	<C> 2018 Weedboi6969@RTEA
*/

#ifndef KERNEL_I8042_H
#define KERNEL_I8042_H

#include <stddef.h>
#include <stdint.h>

extern uint8_t i8042_dualchannel;
extern uint8_t i8042_port0;
extern uint8_t i8042_port1;
extern uint16_t i8042_id0;
extern uint16_t i8042_id1;
extern uint8_t i8042_scs0;
extern uint8_t i8042_scs1;

uint8_t i8042_get_status(void);
void i8042_send_cmd(uint8_t command);
void i8042_send_data(uint8_t data);
uint8_t i8042_get_data(void);
void i8042_send_port(uint8_t port, uint8_t data);
int8_t i8042_init(void);
uint8_t i8042_get_scset(uint8_t port);
int8_t i8042_set_scset(uint8_t port, uint8_t set);

#endif
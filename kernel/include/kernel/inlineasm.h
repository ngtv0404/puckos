/* 
	inlineasm.h - Inline Assembly library
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
	This file contains i386-specific code for basic I/O functions
	and therefore is i386-specific. You should only include this in
	i386-specific files.
*/

#ifndef KERNEL_INLINEASM_H
#define KERNEL_INLINEASM_H

#include <stddef.h>
#include <stdint.h>

void outb(uint16_t port, uint8_t val);
uint8_t inb(uint16_t port);
void io_wait(void);
uint16_t inw(uint16_t port);
uint32_t inl(uint16_t port);
void outw(uint16_t port, uint16_t val);
void outl(uint16_t port, uint32_t val);
void wrmsr(uint32_t msr, uint64_t val);
uint64_t rdmsr(uint32_t msr);

#endif
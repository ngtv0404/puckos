/* 
	kernel.h - Kernel includes and stuff
	Copyright 2018 Weedboi6969@Real Time Engineering Associates.
*/

#ifndef KERNEL_H
#define KERNEL_H

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <kernel/uart.h>
#include <kernel/int32.h>
#include <kernel/gdt32.h>
#include <kernel/idt32.h>
#include <kernel/video.h>
#include <kernel/fonts.h>
#include <kernel/tty.h>
#include <kernel/multiboot.h>
#include <kernel/paging86.h>
#include <kernel/acpi.h>
#include <kernel/kheap.h>
#include <kernel/i8042.h>
#include <kernel/i8259.h>

/* kernel architecture code */
#define ARCH_I386				0
#define ARCH_X86_64				1

#endif
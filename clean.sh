#!/bin/sh
set -e
. ./config.sh

for PROJECT in $PROJECTS; do
  (cd $PROJECT && $MAKE clean)
done

rm -rf sysroot
rm -rf fddir
rm -rf isodir
rm -rf puckos.iso
rm -rf floppy.img